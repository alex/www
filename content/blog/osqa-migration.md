---
title: "Migration von fragen.spline.de"
tags: ['spline', 'osqa', 'login']
date: 2013-05-29T02:00:00+00:00
draft: false

---
Unser Frage- und Antwort-Dienst [fragen.spline.de](http://fragen.spline.de)
basiert auf [OSQA](http://www.osqa.net/). Leider wird diese Software seit
mittlerweile zwei Jahren nicht mehr weitereintwickelt. Deswegen haben wir uns
nach Alternativen umgeschaut und werden in den nächsten Tagen
[fragen.spline.de](http://fragen.spline.de) auf
[askbot](https://github.com/ASKBOT/askbot-devel) migrieren. Um die Migration zu
vereinfachen müssen wir leider die Anmeldung vorübergehend deaktivieren.
Der unangemeldete Lesezugriff sollte soweit funktionieren, dafür notfalls die
Cookies löschen.

**Update - 06.02.2013 19:42**

Askbot wurde erfolgreich aufgesetzt und die Daten sind mittlerweile auch
migriert worden. Falls bei euch Probleme auftreten, meldet diese bitte direkt
an <spline@spline.de>.
