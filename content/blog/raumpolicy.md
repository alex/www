---
title: "Raumpolicy"
tags: ['spline']
date: 2013-09-03T02:00:00+00:00
draft: false

---

Wir haben am Treffen vom 3. September 2013 eine Policy für den Splineraum beschlossen. [Sie ist hier zu finden](http://www.spline.de/raumpolicy/) und gilt ab sofort. Sie ist unter "About" im Header verlinkt.

