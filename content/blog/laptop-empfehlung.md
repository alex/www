---
title: "Laptop-Empfehlung für Erstis"
date: 2022-10-03T16:20:54+02:00
draft: false
aliases:
  - /laptop
  - /blog/2022/03/16/laptop-empfehlung-2022/
---

Pünktlich zum Studium schaffen sich viele einen eigenen Laptop an. Damit ihr nicht den Falschen kauft ein paar Tipps vorweg.

Gerade in den technischen Modulen gehört der Umgang mit Linux Systemen zu den Arbeitsvorraussetzungen. Daher ist es sinnvoll, sich einen kompatiblen Laptop anzuschaffen. Ein iPad reicht nicht, um die wöchentlichen Aufgaben zu bewältigen!

## Allgemeine Mindestanforderungen

- RAM: 
    - min. 8 GB besser 16 GB
- CPU:
    - mindestens 4 Cores mit Hyperthreadding (SMT) also 8 Threads
    - im Idealfall Intel VT-X oder AMD-V support (Virtualisierung)
- GPU: 
    - für das Studium ist keine dedizierte Grafikkarte erforderlich
- Massenspeicher: 
    - SSD mit min. 256 GB (Keine HDD!)
- Display: 
    - Das Display sollte min. FullHD (1080p) sein.
    - Entspiegelte bzw. matte Displays sind sinnvoll um auch mal auf der Wiese in der Sonne arbeiten zu können.

Für den kleinen Geldbeutel empfehlen wir den Kauf eines generalüberholten ThinkPads. Hier gibt es oft zwischen 100-500€ alte Business-Geräte, die euch mit etwas Pflege gut durch das gesamte Studium begleiten können. 

**WICHTIG:** Grundsätzlich raten wir von "Consumer"- oder "Gaming"-Geräten ab. Die meisten Business Notebooks lassen sich leichter warten und gehen nicht so schnell kaputt. Außerdem halten sich Hersteller bei diesen Geräten oft nicht an gängige Standards und erschweren somit den Linux-Support, was euch unnötige Mühe verursacht. Oft lässt sich durch Abwählen des Betriebssystems bei der Konfiguration im Shop zusätzlich Geld sparen (Windows wird im gesamten Studium sowieso nicht benötigt).

Anlaufstellen sind hier Seiten wie eBay Kleinanzeigen oder [Notebookshop Berlin](https://www.notebookshop-berlin.de/Lenovo-Thinkpad-Laptops-gebraucht-Demo-).

Wenn es etwas Neues sein sollte empfielt sich ein aktuelles ThinkPad über Lenovo Campus (Serien T, X, P), ein Dell (Inspiron, Latitude, Precision, XPS), Star Labs, System76 oder Framework Laptop.

Für die komfortabelste Nutzung lohnt es sich vorher auf der [Webseite des Linux Vendor Firmware Service (LVFS)](https://fwupd.org/lvfs/devices/) zu schauen ob das spezifische Modell unterstützt wird.

Wovon wir abraten sind sämtliche Laptops der Firma Acer und Huawei. Der Linux support von Acer Geräten ist im Allgemeinen schlecht und häufig mit viel Bastelei verbunden. Auch Chromebooks sind nicht zu empfehlen. 
