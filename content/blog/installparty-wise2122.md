---
title: "Installparty WiSe 21/22"
date: 2021-10-14T01:18:49+02:00
draft: false
---
So nach längerem Ausbleiben von richtigen Linux Install Parties bei uns an der Uni wird es dieses Wintersemester wieder einen geben. In Zusammenarbeit mit dem Mentoring bekommt ihr am Donnerstag, den 14. Oktober 2021 von 13-15 Uhr frisch gebacken ein Fedora installiert. Aktuelle Referenzen findet ihr [hier](/installparty).
