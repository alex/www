---
title: "Probleme mit Accounts behoben"
tags: ['accounts', 'spline']
date: 2013-04-03T02:00:00+00:00
draft: false

---
Falls ihr versucht habt zwischen gestern Nacht und heute 18:00 Uhr einen Account
auf [accounts.spline.de](https://accounts.spline.de) zu erstellen, habt ihr sicherlich gemerkt, dass
es hier Probleme gab. Grund hierfür war eine fehlerhafte *state-file* von
logrotate. Dies führte dazu  dass alle Inodes des Systems aufgebraucht wurden.
Mittlerweile sollte alles wieder funktionieren. Falls ihr weiterhin
Unregelmäßigkeiten oder andere Dinge bemerket, meldet dies bitte per Mail an
**spline@spline.de.**
