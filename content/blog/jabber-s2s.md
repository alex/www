---
title: "jabber.spline.de s2s encryption"
tags: ['jabber', 'security']
date: 2014-01-02T02:00:00+00:00
draft: false

---

Spline schaltet am Wochenende s2s encryption an, das kann und wird Probleme mit google erzeugen.

---

There has been a long discussion about how to handle server to server
encryption or the lack of it on the [xmpp operators mailing
list](http://mail.jabber.org/mailman/listinfo/operators). Admins would like to
switch it on of course but the biggest player in the federation network,
Google, is blocking this.
However, both admins and developers have created and signed a manifesto on how
to deal with this issue that puts pressure on Google and might in the worst
case result in a federation split and the loss of a huge user base:

<br>
[https://github.com/stpeter/manifesto/blob/master/manifesto.txt](https://github.com/stpeter/manifesto/blob/master/manifesto.txt)
<br>

I, personally, fully support this manifesto and hence decided to play along and switch to:

    -{s2s_use_starttls, optional}.
    +{s2s_use_starttls, required_trusted}.

on Saturday, January 4th. Let's see how it goes :)

