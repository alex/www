---
title: "experimentelles Git-Hosting"
tags: ['service', 'spit', 'git']
date: 2011-11-02T02:00:00+00:00
draft: false

---
Wir haben einen neuen Git-Hosting-Dienst. Zu finden ist dieser unter
[spit.spline.de](http://spit.spline.de).
