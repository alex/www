---
title: "Dev offiziell als Service"
tags: ['service', 'dev']
date: 2008-10-16T02:00:00+00:00
draft: false

---
Liebe Mitglieder des Fachbereichs,

wir möchten auf unseren neuen Service aufmerksam machen:

[https://dev.spline.de](https://dev.spline.de)

Er ersetzt SplineForge und ist quasi dasselbe, nur besser. SplineForge
wird im Gegenzug vom Netz genommen.

dev.spline.de freut sich auf rege Benutzung!
