---
title: "Gitlab aktualisiert auf v5.2"
tags: ['spline', 'gitlab', 'service']
date: 2013-06-02T02:00:00+00:00
draft: false

---
Soeben wurde [Gitlab](http://gitlab.spline.de), unser git-Dienst, auf die
neuste Version 5.2 gebracht. Mit dem Update gibt es unter anderem folgende
Verbesserungen:

* Projekte können nun in den eigenen Namespace *geforked* werden.
* Suche berücksichtigt nun auch Quelltexte

Einen detaillierten Changelog gibts
[hier](http://blog.gitlab.org/gitlab-5-dot-2-released/).
